FROM node:12.18.1
ENV NODE_ENV=develop

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install

COPY . .

CMD [ "node", "palindrome.js" ]
